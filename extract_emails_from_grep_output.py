import re
import os
home = os.path.expanduser("~")
grep_output = open(os.path.join(home, 'output.txt'))

emails_only_file = open(os.path.join(home, 'emails.txt'), "w")

for idx, line in enumerate(grep_output):
    match = re.search(r'[\w\.-]+@[\w\.-]+', line)
    if match:
        emails_only_file.write(match.group(0) + "\n")

print("%d emails matched" % idx)
