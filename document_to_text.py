import os

f = []
for (dirpath, dirnames, filenames) in os.walk("."):
    f.extend(filenames)
    break

path = os.getcwd()
for idx, filename in enumerate(f):
    print(filename)
    if filename.endswith(".doc"):
        new_filename = "%d.doc" % idx
        os.rename(os.path.join(path, filename), os.path.join(path, new_filename))
        os.system("catdoc {0} > {0}.txt".format(new_filename))
    elif filename.endswith(".docx"):
        new_filename = "%d.docx" % idx
        os.rename(os.path.join(path, filename), os.path.join(path, new_filename))
        os.system("docx2txt {0}".format(new_filename))
    elif filename.endswith(".pdf"):
        new_filename = "%d.pdf" % idx
        os.rename(os.path.join(path, filename), os.path.join(path, new_filename))
        os.system("pdftotext {0}".format(new_filename))

print("converted %d files" % len(f))
