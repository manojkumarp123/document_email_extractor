# install packages
sudo apt-get install catdoc
sudo apt-get install docx2txt
sudo apt-get install poppler-utils

# extract file names and convert them to text
python document_to_text.py
# scan for emails and save to ~/output.txt
grep -E -o -r "\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}\b" . > ~/output.txt
echo "temporary file ~/output.txt created"
python extract_emails_from_grep_output.py
echo "emails ~/emails.txt created"
